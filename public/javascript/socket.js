var socket = io('http://localhost:3000/');
// var socket = io('/my-namespace');
// socket.on('news', function (data) {
// 	console.log(data);
// 	socket.emit('my other event', 'Client to Server');
// });
var x = 1;
var i;
var turn;
var idUser = 0;
var col = 0, row = 0; cross1 = 0; cross2 = 0;
var countClick = 0;
var rowCurrent = -1, ceilCurrent = -1;
var rowInd, cellInd;
var btn1 = $('#btn1');
var btn2 = $('#btn2');
var pause;
var td = document.getElementsByTagName('td');
// var name = '';
var User = [];
var timeup = 30;
// var btn1 = document.getElementById('btn1');

socket.on('connect', function () { // TIP: you can avoid listening on `connect` and listen on events directly too!
    socket.emit('ferret', 'Dat','woot', function (data) { // args are sent in order to acknowledgement function
      console.log(data); // data will be 'tobi says woot'
      User.push(data); // ???
      // console.log(User);
    });
  });

// người chơi click vào ô "Bắt đầu"
btn1.click(function() {
    // name=document.getElementById('a').value;
    socket.emit('click', sessionStorage.getItem("id"));
    // console.log(sessionStorage.getItem("id"))
})
// người chơi nhận giao diện trò chơi
socket.on('clickall', function(table,id) {
    // console.log(User)
    // console.log(123)
    // console.log(td)
    
    document.getElementById("caro").innerHTML = table; 
    // người chơi click vào ô bất kì
    // console.log(User);
    console.log(id)
    document.getElementById("time").innerHTML = "30";
    start();
    if(x == 1 && id == User[0]) {
        turn = 1;
        document.getElementById("turn").innerHTML = "Lượt của bạn"
    }
    else {
        turn = 0;
        document.getElementById("turn").innerHTML = "Lượt đối phương"
    }

    document.getElementById('table').addEventListener('click', function(e) {
        if(x == 0 && id == User[0]) return false;
        if(x == 1 && id != User[0]) return false;
        // console.log(e);
        if (e.path[0].tagName == 'TD' && e.path[0].innerHTML == "") {
            if(x % 2) {
                e.target.innerHTML = "X";
                countClick++;
            }
            else {
                e.target.innerHTML = "O";
                countClick++;
            }
            x = ++x%2;
        } else return false;
        console.log(sessionStorage.getItem("id"));
        // người chơi gửi dữ liệu ô vừa click
        socket.emit('clicksquare', {id : id, id_user: sessionStorage.getItem("id"),tr : e.path[1].rowIndex,td : e.path[0].cellIndex,text : e.path[0].innerHTML, x : x, rowCur : rowCurrent, cellCur : ceilCurrent, caro : caro,countClick : countClick})
        stop();
        
    });
})
// socket.emit('clicksquare');
// người chơi luôn nhận state màn chơi từ server
// người chơi luôn tự động cập nhật điểm số để chiến thắng
// người chơi tự động thông báo finish và draw khi kết thúc
socket.on('allsquare',function(data,count, row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22, player_win) {
    // console.log(data ,count, row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22);
    // countClick++;
    stop();
    // var pause_var = 0;
    // document.querySelector('#pause').addEventListener('click',function() {
    //     socket.emit('pause');
    //     pause_var = ++pause_var%2;
    // })
    // socket.on('pause_next',function() {
    //     console.log(pause_var)
    //     if(pause_var) {
    //         stop();
    //         document.getElementById("pause").innerHTML = "Continue"
    //     }
    //     else {
    //         start();
    //         document.getElementById("pause").innerHTML = "Pause"
    //     }
    // })
    timeup = 30;
    turn = ++turn%2;
    if(turn == 1)
        document.getElementById("turn").innerHTML = "Lượt của bạn"
    else
        document.getElementById("turn").innerHTML = "Lượt đối phương"
    start();
    countClick = data.countClick;
    // console.log(countClick)
    // console.log(data.x);
    x = data.x;
    rowCurrent = data.tr;
    ceilCurrent = data.td;
    td[data.tr * 10 + data.td].innerHTML = data.text; 
    draw(data.tr, data.td, data.rowCur, data.cellCur);
    if(row == 4) {
        td[data.tr * 10 + data.td].style.backgroundColor = "red";
        for(i = 1; i <= countRow1;i++) {
            td[data.tr * 10 + data.td - i].style.backgroundColor = "red";
        }
        for(i = 1; i <= countRow2;i++) {
            td[data.tr * 10 + data.td + i].style.backgroundColor = "red";
        }
    }
    if(col == 4) {
        td[data.tr * 10 + data.td].style.backgroundColor = "red";
        for(i = 1; i <= countCol1;i++) {
            td[(data.tr - i) * 10 + data.td].style.backgroundColor = "red";
        }
        for(i = 1; i <= countCol2;i++) {
            td[(data.tr + i) * 10 + data.td].style.backgroundColor = "red";
        }
    }
    if(cross1 == 4) {
        td[data.tr * 10 + data.td].style.backgroundColor = "red";
        for(i = 1; i <= countCross11;i++) {
            td[(data.tr - i) * 10 + data.td - i].style.backgroundColor = "red";
        }
        for(i = 1; i <= countCross12;i++) {
            td[(data.tr + i) * 10 + data.td + i].style.backgroundColor = "red";
        }
    }
    if(cross2 == 4) {
        td[data.tr * 10 + data.td].style.backgroundColor = "red";
        for(i = 1; i <= countCross21;i++) {
            td[(data.tr - i) * 10 + data.td + i].style.backgroundColor = "red";
        }
        for(i = 1; i <= countCross22;i++) {
            td[(data.tr + i) * 10 + data.td - i].style.backgroundColor = "red";
        }
    }

    //kiểm tra kết thúc và thông báo kết thúc
    if(countClick > 7 ) if(count) {document.getElementById("table").classList.remove("table"); setTimeout(() => {
        console.log(player_win);
        if(data.x == 0) {
            if(data.id == User[0]) {
                window.alert("BẠN THẮNG"); 
            }
            else {
                window.alert("BẠN THUA"); 

            }
        }
        else 
            if(data.id == User[0]) {
                window.alert("BẠN THUA"); 

            }
            else {
                window.alert("BẠN THẮNG"); 

            }
        console.log(player_win == sessionStorage.getItem("id"));
        if(player_win == sessionStorage.getItem("id")) {
            window.location = "/finish";
            console.log("Win keets thuc")
        }
        else {
            location.reload();
            // console.log("lose")
        }
        // window.location = "/finish";
        // location.reload();
    }, 300);
    }

    //Xét trường hợp hết ô trống
    if(countClick > 90) {document.getElementById("table").classList.remove("table"); setTimeout(() => {
        if(data.x == 0) {
            if(data.id == User[0]) {
                window.alert("BẠN HÒA"); 
            }
            else {
                window.alert("BẠN HÒA"); 

            }
        }
        else 
            if(data.id == User[0]) {
                window.alert("BẠN HÒA"); 

            }
            else {
                window.alert("BẠN HÒA"); 

            }
        // location.reload();
    }, 300);
    }
    // socket.emit('finish', document.getElementsByTagName('td') );
})



// pause.click(function() {
//     socket.emit('pause');
//     console.log("pause")
// })
// socket.on('pause_next',function() {
//     console.log("pause_next")
//     stop();
// })
draw=(rowInd, cellInd, rowCur, cellCur)=> {
    if(rowCur >= 0) {
        td[rowCur * 10 + cellCur].style.backgroundColor = "";
    }
    // rowCurrent = rowInd;
    // ceilCurrent = cellInd;
    td[rowInd * 10 + cellInd].style.backgroundColor = "gray";
}


btn2.click(function() {
    // name=document.getElementById('a').value;
    socket.emit('click_ai', 'click_ai')
})
// người chơi nhận giao diện trò chơi
socket.on('clickall_ai', function(table,id) {
    document.getElementById("caro").innerHTML = table; 
    // người chơi click vào ô bất kì
    document.getElementById('table').addEventListener('click', function(e) {
        if(x == 0 && id == User[0]) return false;
        if(x == 1 && id != User[0]) return false;
        console.log(e);
        if (e.path[0].tagName == 'TD' && e.path[0].innerHTML == "") {
            if(x % 2) {
                e.target.innerHTML = "X";
                countClick++;
            }
            else {
                e.target.innerHTML = "O";
                countClick++;
            }
            x = ++x%2;
        } else return false;
        // người chơi gửi dữ liệu ô vừa click
        socket.emit('clicksquare_ai', {id : id,tr : e.path[1].rowIndex,td : e.path[0].cellIndex,text : e.path[0].innerHTML, x : x, rowCur : rowCurrent, cellCur : ceilCurrent, caro : caro})
        
    });
})
// socket.emit('clicksquare');
// người chơi luôn nhận state màn chơi từ server
// người chơi luôn tự động cập nhật điểm số để chiến thắng
// người chơi tự động thông báo finish và draw khi kết thúc
socket.on('allsquare_ai',function(data,count, row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22) {
    // console.log(data ,count, row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22);
    countClick++;
    // console.log(countClick)
    // console.log(data.x);
    x = data.x;
    rowCurrent = data.tr;
    ceilCurrent = data.td;
    td[data.tr * 10 + data.td].innerHTML = data.text; 
    draw(data.tr, data.td, data.rowCur, data.cellCur);
    if(row == 4) {
        td[data.tr * 10 + data.td].style.backgroundColor = "red";
        for(i = 1; i <= countRow1;i++) {
            td[data.tr * 10 + data.td - i].style.backgroundColor = "red";
        }
        for(i = 1; i <= countRow2;i++) {
            td[data.tr * 10 + data.td + i].style.backgroundColor = "red";
        }
    }
    if(col == 4) {
        td[data.tr * 10 + data.td].style.backgroundColor = "red";
        for(i = 1; i <= countCol1;i++) {
            td[(data.tr - i) * 10 + data.td].style.backgroundColor = "red";
        }
        for(i = 1; i <= countCol2;i++) {
            td[(data.tr + i) * 10 + data.td].style.backgroundColor = "red";
        }
    }
    if(cross1 == 4) {
        td[data.tr * 10 + data.td].style.backgroundColor = "red";
        for(i = 1; i <= countCross11;i++) {
            td[(data.tr - i) * 10 + data.td - i].style.backgroundColor = "red";
        }
        for(i = 1; i <= countCross12;i++) {
            td[(data.tr + i) * 10 + data.td + i].style.backgroundColor = "red";
        }
    }
    if(cross2 == 4) {
        td[data.tr * 10 + data.td].style.backgroundColor = "red";
        for(i = 1; i <= countCross21;i++) {
            td[(data.tr - i) * 10 + data.td + i].style.backgroundColor = "red";
        }
        for(i = 1; i <= countCross22;i++) {
            td[(data.tr + i) * 10 + data.td - i].style.backgroundColor = "red";
        }
    }
    if(countClick > 7 ) if(count) {document.getElementById("table").classList.remove("table"); setTimeout(() => {
        if(data.x == 0) {
            if(data.id == User[0]) 
                window.alert("BẠN THẮNG"); 
            else 
                window.alert("BẠN THUA"); 
        }
        else 
            if(data.id == User[0])
                window.alert("BẠN THUA"); 
            else
                window.alert("BẠN THẮNG"); 
        location.reload()}, 300);
    }
    // socket.emit('machine', data);
    // socket.emit('finish', document.getElementsByTagName('td') );
})

// socket.on('allsquare_ai2',function(data,count, row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22) {
//     // console.log(data ,count, row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22);
//     countClick++;
//     // console.log(countClick)
//     // console.log(data.x);
//     x = data.x;
//     rowCurrent = data.tr;
//     ceilCurrent = data.td;
//     td[data.tr * 10 + data.td].innerHTML = data.text; 
//     draw(data.tr, data.td, data.rowCur, data.cellCur);
//     if(row == 4) {
//         td[data.tr * 10 + data.td].style.backgroundColor = "red";
//         for(i = 1; i <= countRow1;i++) {
//             td[data.tr * 10 + data.td - i].style.backgroundColor = "red";
//         }
//         for(i = 1; i <= countRow2;i++) {
//             td[data.tr * 10 + data.td + i].style.backgroundColor = "red";
//         }
//     }
//     if(col == 4) {
//         td[data.tr * 10 + data.td].style.backgroundColor = "red";
//         for(i = 1; i <= countCol1;i++) {
//             td[(data.tr - i) * 10 + data.td].style.backgroundColor = "red";
//         }
//         for(i = 1; i <= countCol2;i++) {
//             td[(data.tr + i) * 10 + data.td].style.backgroundColor = "red";
//         }
//     }
//     if(cross1 == 4) {
//         td[data.tr * 10 + data.td].style.backgroundColor = "red";
//         for(i = 1; i <= countCross11;i++) {
//             td[(data.tr - i) * 10 + data.td - i].style.backgroundColor = "red";
//         }
//         for(i = 1; i <= countCross12;i++) {
//             td[(data.tr + i) * 10 + data.td + i].style.backgroundColor = "red";
//         }
//     }
//     if(cross2 == 4) {
//         td[data.tr * 10 + data.td].style.backgroundColor = "red";
//         for(i = 1; i <= countCross21;i++) {
//             td[(data.tr - i) * 10 + data.td + i].style.backgroundColor = "red";
//         }
//         for(i = 1; i <= countCross22;i++) {
//             td[(data.tr + i) * 10 + data.td - i].style.backgroundColor = "red";
//         }
//     }
//     if(countClick > 7 ) if(count) {document.getElementById("table").classList.remove("table"); setTimeout(() => {
//         if(data.x == 0) {
//             if(data.id == User[0]) 
//                 window.alert("BẠN THẮNG"); 
//             else 
//                 window.alert("BẠN THUA"); 
//         }
//         else 
//             if(data.id == User[0])
//                 window.alert("BẠN THUA"); 
//             else
//                 window.alert("BẠN THẮNG"); 
//         location.reload()}, 300);
//     }
//     // socket.emit('finish', document.getElementsByTagName('td') );
// })
function start()
{
    if (timeup == -1){
        clearTimeout(timeout);
        var text;
        countClick++;
        if(turn == 1 ) {
            // alert('Hết giờ');
            // console.log(x)
            if(x) 
                text = "O"
            else
                text = "X"
            console.log( x + "  " +text)
            socket.emit('timeup', {x : x, text: text, rowCur : rowCurrent, cellCur : ceilCurrent, caro : caro,countClick : countClick});
        }
        return false;
    }

    /*BƯỚC 1: HIỂN THỊ ĐỒNG HỒ*/
    document.getElementById('time').innerHTML = timeup.toString();
    timeout = setTimeout(function(){
        timeup--;
        start();
    }, 1000);
}

function stop(){
    clearTimeout(timeout);
}