var gulp = require('gulp');
var fileinclude = require('gulp-file-include');

gulp.task('fileinclude', function() {
	return gulp.src('./public/html/*.html')
		.pipe(fileinclude({
			prefix : '@@',
			basepath : '@file'
		}))
		.pipe(gulp.dest('./public//'))
})
gulp.task('default', gulp.series(['fileinclude']));
