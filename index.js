//Server

var express= require('express');
var app = express();
var mysql = require('mysql');
var http = require('http').Server(app);
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var io = require('socket.io')(http);
// io = io.of('/caro');
var profile = io.of('/profile')
// var nsp = io.of('/my-namespace');
var row, col, cross1, cross2;
var countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22;

var user = [];
var user_access = [];
var var_access = 0;
var player_win, player_lose;
// console.log(window.sessionStorage)
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/public/login.html');
})
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'caro'
});

app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
// app.use(express.json());       // to support JSON-encoded bodies
// app.use(express.urlencoded()); // to support URL-encoded bodies

app.set('view engine', 'pug');
app.set('views', './views');

app.get('/test', function(req, res) {
  res.render('caro')
})

app.post('/login', function(request, response) {
  request.session.dat = "124";
  // response.render('caro');
  var username = request.body.username;
  var password = request.body.password;
  if (username && password) {
    connection.query('SELECT * FROM user WHERE username = ? AND password = ?', [username, password], function(error, results, fields) {
      if (results.length > 0) {
        request.session.loggin = true;
        request.session.username = username;
        var temp = 0;
        for(var i=0; i < user.length; i++) {
          if(user[i][0] == results[0].id)
            temp = 1;
        }
        if(temp == 1) {
          response.status(200).send({type : 2});
        }
        else {
        // window.sessionStorage.setItem("username", username);
        user.push([results[0].id, username, results[0].email, results[0].name])
        // response.status(200).redirect('/caro');
        // response.end();
        // response.render('caro');

        response.status(200).send({url: "/caro.html", username : username, name : results[0].name,id : results[0].id, email: results[0].email, password: password, type: 1})
        }
      } else {
        response.status(200).send({message: "thông tin sai", type: 0})
      }     
      response.end();
    });
  } 
});



app.get('/caro', function(req, res){
  if(req.session.loggin) {
    res.render('caro', {
      username : req.session.username
    });
  }
  else
    res.redirect('/')
  res.end();
})

app.get('/profile', function(req, res) {
  res.render('profile');
})

app.get('/account', function(req, res) {
  res.render('account');
})

app.get('/setting', function(req, res) {
  res.render('account');
})

app.get('/result', function(req, res) {
  res.render('account');
})

app.get('/change', function(req, res) {
  res.render('change')
})

app.post('/changeok', function(req, res) {
  // console.log(username);
   console.log(req.body);
   var query = 'UPDATE user SET password = "'+ req.body.password + '", name = "' + req.body.name + '", email = "' + req.body.email + '" WHERE username = "' + req.body.username +'"';
   console.log(query);
   connection.query(query, function(error, results, fields) {
     if(error) console.log(error.message)
   })
  res.redirect('/account')
})

app.get('/logout', function(req, res) {
  req.session.loggin = false;
  res.redirect('/')
})

app.post('/register', function(req, res) {
  var username = req.body.username;
  var password = req.body.password;
  var name = req.body.name;
  var email = req.body.email;
  if (username && password) {
    var bool = connection.query('SELECT * FROM user ', function(error, results, fields) {
      var length, temp;
      length = results.length;
      for( i = 0; i< length; i++) {
        if(results[i].username == username) {
          res.status(200).send({type : 0});
          temp ++;
          res.end();
          return false;
        }
      }
      connection.query('INSERT INTO user ( id , username , password , name , email ) VALUES ( ? , ? , ? , ? , ? )',[ length + 1 , username, password, name, email], function(error, results, fields) {
        if(error) console.log(error.message)
          res.status(200).send({type : 1, id: length + 1, username: username, password: password, name: name, email: email});
        req.session.loggin = true;
      });
    });

  }
  
})

app.get('/finish', function(req, res) {
  var time = new Date();
  var query = 'INSERT INTO history ( player_win , player_lose , time ) VALUES ( '+ player_win+' , '+player_lose+' , "'+ time.toGMTString()+'" )';
  console.log(query);
  connection.query( query, function(error, results, fields) {
    if(error) console.log(error.message)
  });
  console.log("Ok finish")
  res.redirect('/caro')
})

app.use(express.static('public'))


io.on('connection', function(socket) {
  caro = [];
  for(var i = 0; i< 100;i++) {
    caro.push("");
  }
  socket.broadcast.emit('User conenct')
  console.log('A user connected.' + socket.id);
  socket.on('disconnect', function() {
    console.log('User disconnected' + socket.id);
  });
  socket.on('ferret', function (name, word, fn) {
    fn(socket.id);
  });
  // server nhận yêu cầu "Bắt đầu" từ 1 người chơi
  // sau đó server gửi giao diện trò chơi cho mọi người chơi
  socket.on('click', function(data) {
    io.sockets.emit('clickall', table(10), socket.id);
    console.log(data);
  })
  //server nhận dữ liệu click 1 ô của 1 người chơi
  //sau đó server gửi state mới của màn chơi cho tất cả người chơi (kể cả người click -_- mặc kệ trùng)
  socket.on('clicksquare',function(data) {
    var player_finish = false;
    // console.log(data);
    caro[data.tr * 10 + data.td] = data.text;
    // console.log(caro);
    // data.tr = 5;
    // data.td = 5;
    if(count(caro,data.tr, data.td, 10, data.text) == true) {
      // console.log("finish")
      player_win = data.id_user;
      // console.log(user);
      // console.log(player_win);
      // console.log(user[0].indexOf(player_win));
      if(user[0][0] == player_win)
        player_lose = user[1][0];
      else
        player_lose = user[0][0];
      console.log("player_win  :" + player_win );
      console.log("player_lose :" + player_lose );
      player_finish = true;
    }
    io.sockets.emit('allsquare', data,count(caro,data.tr, data.td, 10, data.text), row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22 , player_win);
    if(count(caro,data.tr, data.td, 10, data.text)) return false;

    // var best = bestScore(caro);
    // console.log(best);
    // data.tr = best[0], data.td = best[1];
    // // data.tr = data.tr + 1;
    // data.x = (data.x + 1) % 2;
    // data.text = data.text == 'X' ? 'O' : 'X';
    // caro[data.tr * 10 + data.td] = data.text;
    // io.sockets.emit('allsquare', data,count(caro,data.tr, data.td, 10, data.text), row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22 );

  })
  socket.on('timeup', function(data) {
    var best = bestScore(caro);
    data.tr = best[0], data.td = best[1];
    // data.tr = data.tr + 1;
    // console.log(data.x)
    data.x = (data.x + 1) % 2;
    data.text = data.text == 'X' ? 'O' : 'X';
    caro[data.tr * 10 + data.td] = data.text;
    if(count(caro,data.tr, data.td, 10, data.text) == true) {
      // console.log("finish")
      player_win = data.id_user;
      // console.log("player_win  :" +player_win );
    }
    io.sockets.emit('allsquare', data,count(caro,data.tr, data.td, 10, data.text), row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22 );
  })



  socket.on('click_ai', function(data) {
    // console.log(data);
    io.sockets.emit('clickall_ai', table_ai(10), socket.id);
  })
  //server nhận dữ liệu click 1 ô của 1 người chơi
  //sau đó server gửi state mới của màn chơi cho tất cả người chơi (kể cả người click -_- mặc kệ trùng)
  socket.on('clicksquare_ai',function(data) {
    // console.log(data);
    caro[data.tr * 10 + data.td] = data.text;
    // console.log(caro);
    // data.tr = 5;
    // data.td = 5;
    io.sockets.emit('allsquare_ai', data,count(caro,data.tr, data.td, 10, data.text), row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22 );
    if(count(caro,data.tr, data.td, 10, data.text)) return false;

    var best = bestScore(caro);
    // console.log(best);
    data.tr = best[0], data.td = best[1];
    // data.tr = data.tr + 1;
    data.x = (data.x + 1) % 2;
    data.text = data.text == 'X' ? 'O' : 'X';
    caro[data.tr * 10 + data.td] = data.text;
    // setTimeout(()=>{
      
    io.sockets.emit('allsquare_ai', data,count(caro,data.tr, data.td, 10, data.text), row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22 );
    // },1000)

  })

  // socket.on('machine', function(data) {
  //   var best = bestScore(caro);
  //   console.log(best);
  //   data.tr = best[0], data.td = best[1];
  //   // data.tr = data.tr + 1;
  //   data.x = (data.x + 1) % 2;
  //   data.text = data.text == 'X' ? 'O' : 'X';
  //   caro[data.tr * 10 + data.td] = data.text;
  //   // setTimeout(()=>{
      
  //   io.sockets.emit('allsquare_ai2', data,count(caro,data.tr, data.td, 10, data.text), row, col, cross1, cross2, countRow1, countRow2, countCol1, countCol2, countCross11, countCross12, countCross21, countCross22 );
  // })
  // socket.on('finish',function(data) {
  //   console.log(data)
  // })
  socket.on('pause',function() {
    io.sockets.emit('pause_next')
  })
})



profile.on('connection', function(socket) {
})

http.listen(3000, function() {
  console.log('Start')
})

table=(n)=> {
  var html = '';
  html += '<div id="turn" style="margin-top: -10px; text-align:center; margin-bottom: 5px; font-size:200%;"></div>'

  html += '<div style="position:relative; width:330px; margin: 0 auto;">'
  // html += '<div id="pause" style="position: absolute;top: 4px;width: 90px; text-align: center;left: 220px;font-size: 24px;background-color: white;cursor: pointer;height: 30px;"> Pause </div>'
  html += '<div style="position: absolute;top: 0px;left: 183px;font-size: 200%;"> s </div>'
  html += '<div id="time" style="text-align:center; margin-bottom: 15px; font-size:200%;"></div>'
  html += '</div>'
  
  html += '<table id="table" class="table">';
  var count=n;
  for(var i=0;i<count; i++){
      html+='<tr>';
      for(var j=0; j<count; j++){
          html+='<td class="square"></td>';
      }
      html+='</tr>';
  }
  html+='</table>';
  return html;
}
table_ai=(n)=> {
  var html = '';
  // html += '<div id="turn" style="margin-top: -10px; text-align:center; margin-bottom: 5px; font-size:200%;"></div>'

  // html += '<div style="position:relative; width:330px; margin: 0 auto;">'
  // // html += '<div id="pause" style="position: absolute;top: 4px;width: 90px; text-align: center;left: 220px;font-size: 24px;background-color: white;cursor: pointer;height: 30px;"> Pause </div>'
  // html += '<div style="position: absolute;top: 0px;left: 183px;font-size: 200%;"> s </div>'
  // html += '<div id="time" style="text-align:center; margin-bottom: 15px; font-size:200%;"></div>'
  // html += '</div>'
  
  html += '<table id="table" class="table">';
  var count=n;
  for(var i=0;i<count; i++){
      html+='<tr>';
      for(var j=0; j<count; j++){
          html+='<td class="square"></td>';
      }
      html+='</tr>';
  }
  html+='</table>';
  return html;
}
count=(td, rowInd, cellInd, maxInd, XO)=> {
  // rowCurrent = rowInd;
  // ceilCurrent = cellInd;
  // e.path[0].bgColor = "gray";
  countRow1 = 0, countRow2 = 0, countCol1 = 0, countCol2 = 0, countCross11 = 0, countCross12 = 0, countCross21 = 0, countCross22 = 0;
  row = 0; col = 0; cross1 = 0; cross2 = 0;
  var r = countR(td,row, countRow1, countRow2, cellInd, rowInd, maxInd, XO);
  row = r[0];
  countRow1 = r[1];
  countRow2 = r[2];
  
  var co = countCol(td,col, countCol1, countCol2, cellInd, rowInd, maxInd, XO);
  col = co[0];
  countCol1 = co[1];
  countCol2 = co[2];

  var cr1 = countCro1(td,cross1, countCross11, countCross12, cellInd, rowInd, maxInd, XO);
  cross1 = cr1[0];
  countCross11 = cr1[1];
  countCross12 = cr1[2];
  
  var cr2 = countCro2(td, cross2, countCross21, countCross22, cellInd, rowInd, maxInd, XO);
  cross2 = cr2[0];
  countCross21 = cr2[1];
  countCross22 = cr2[2];
  
  if(row == 4 || col == 4 || cross1 == 4 || cross2 == 4) return true;
}

bestScore=(caro)=>{
  var best = -1;
  var index1 = -1;
  var index2 = -1;
  for( var i = 0; i < 10; i++) {
    for( var j = 0 ; j < 10; j++) {
      if(caro[i * 10 + j] == 'X' || caro[i * 10 + j] == 'O') continue;
      var temp11 = countR(caro, 0, 0, 0, j, i, 10, 'X')
      var temp12 = countCol(caro, 0, 0, 0, j, i, 10, 'X')
      var temp13 = countCro1(caro, 0, 0, 0, j, i, 10, 'X')
      var temp14 = countCro2(caro, 0, 0, 0, j, i, 10, 'X')
      var temp21 = countR(caro, 0, 0, 0, j, i, 10, 'O')
      var temp22 = countCol(caro, 0, 0, 0, j, i, 10, 'O')
      var temp23 = countCro1(caro, 0, 0, 0, j, i, 10, 'O')
      var temp24 = countCro2(caro, 0, 0, 0, j, i, 10, 'O')
      var x = temp11[0] * temp11[0] + temp12[0] * temp12[0] + temp13[0] * temp13[0] + temp14[0] * temp14[0] + temp21[0] * temp21[0] + temp22[0] * temp22[0] + temp23[0] * temp23[0] + temp24[0] * temp24[0]
      if(x > best) {
        best = x;
        index1 = i;
        index2 = j;
      }
    }
  }
  return [index1, index2]
}

countR=(td, row, countRow1, countRow2, cellInd, rowInd, maxInd, XO)=> {
  for(i = cellInd - 1;i >=0;i --) {
    if(td[rowInd * 10 + i] == XO) 
      countRow1 ++;
    else break;
  }
  for(i = cellInd + 1; i < maxInd; i++) {
    if(td[rowInd * 10 + i] == XO) countRow2 ++;
    else break;
  }
  row = countRow1 + countRow2;
  return [row, countRow1, countRow2]
}
countCol=(td, col, countCol1, countCol2, cellInd, rowInd, maxInd, XO)=> {
  for(i = rowInd - 1; i>=0; i--) {
    if(td[i * 10 + cellInd] == XO) countCol1 ++;
    else break;
  }
  for(i = rowInd + 1; i<maxInd; i++) {
    if(td[i * 10 + cellInd] == XO) countCol2 ++;
    else break;
  }
  col = countCol1 + countCol2;
  return [col, countCol1, countCol2]
}
countCro1=(td, cross1, countCross11, countCross12, cellInd, rowInd, maxInd, XO)=> {
  for(i = rowInd - 1,j = cellInd - 1;;) {
    if(i<0 || j < 0) break;
    if(td[i * 10 + j] == XO) countCross11 ++;
    else break;
    i--; j--;
  }
  for(i = rowInd + 1,j = cellInd + 1; ; ) {
    if(i == maxInd || j == maxInd) break;
    if(td[i * 10 + j] == XO) countCross12 ++;
    else break;
    i ++; j++;
  }
  cross1 = countCross11 + countCross12;
  return [cross1, countCross11, countCross12]
}
countCro2=(td, cross2, countCross21, countCross22, cellInd, rowInd, maxInd, XO)=> {
  for(i = rowInd - 1,j = cellInd + 1;;) {
    if(i<0 || j == maxInd) break;
    if(td[i * 10 + j] == XO) countCross21 ++;
    else break;
    i--; j++;
  }
  for(i = rowInd + 1,j = cellInd - 1; ; ) {
    if(i == maxInd || j < 0) break;
    if(td[i * 10 + j] == XO) countCross22 ++;
    else break;
    i ++; j--;
  }
  cross2 = countCross21 + countCross22;
  return [cross2, countCross21, countCross22]
}
